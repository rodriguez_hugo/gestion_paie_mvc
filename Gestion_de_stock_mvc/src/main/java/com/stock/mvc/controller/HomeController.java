package com.stock.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entity.Salarie;
import com.stock.mvc.services.ISalarieService;

@Controller
@RequestMapping(value="/", method = RequestMethod.GET)
public class HomeController {
	@RequestMapping(value="")
	public String home() {
		return "home/home";
	}
	
	@RequestMapping(value="/blank")
	public String blank() {
		return "blank/blank";
	}
	
	@RequestMapping(value="/employeur")
	public String employeur() {
		return "employeur/employeur";
	}
	
	@RequestMapping(value="/rembrute")
	public String rembrute() {
		return "rembrute/rembrute";
	}
	
	@RequestMapping(value="/impot")
	public String impot() {
		return "impot/impot";
	}
	
	@RequestMapping(value="/salaire")
	public String salaire() {
		return "salaire/salaire";
	}
	
	@RequestMapping(value="/sante")
	public String sante() {
		return "sante/sante";
	}
}
