package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.Impot;

public interface IImpotDao {

	Impot save(Impot entity);

	Impot update(Impot entity);

	List<Impot> selectAll();

	List<Impot> selectAll(String sortField, String sort);

	Impot getById(Long id);

	void remove(Long id);

	Impot findOne(String paramName, String paramValue);

	Impot findOne(String[] paramNames, Object[] paramValues);

	int findCountBy(String paramName, String paramValue);

}
