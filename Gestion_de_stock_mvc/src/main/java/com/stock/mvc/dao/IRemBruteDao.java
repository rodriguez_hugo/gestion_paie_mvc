package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.RemBrute;

public interface IRemBruteDao {

	RemBrute save(RemBrute entity);

	RemBrute update(RemBrute entity);

	List<RemBrute> selectAll();

	List<RemBrute> selectAll(String sortField, String sort);

	RemBrute getById(Long id);

	void remove(Long id);

	RemBrute findOne(String[] paramNames, Object[] paramValues);

	RemBrute findOne(String paramName, String paramValue);

	int findCountBy(String paramName, String paramValue);

}
