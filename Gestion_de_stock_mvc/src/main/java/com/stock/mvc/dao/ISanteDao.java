package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.Sante;

public interface ISanteDao {

	Sante save(Sante entity);

	Sante update(Sante entity);

	List<Sante> selectAll();

	List<Sante> selectAll(String sortField, String sort);

	Sante getById(Long id);

	void remove(Long id);

	Sante findOne(String[] paramNames, Object[] paramValues);

	Sante findOne(String paramName, String paramValue);

	int findCountBy(String paramName, String paramValue);

}
