package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entity.Employeur;

public interface IEmployeurDao {

	Employeur save(Employeur entity);

	Employeur update(Employeur entity);

	List<Employeur> selectAll();

	List<Employeur> selectAll(String sortField, String sort);

	Employeur getById(Long id);

	void remove(Long id);

	Employeur findOne(String[] paramNames, Object[] paramValues);

	Employeur findOne(String paramName, String paramValue);

	int findCountBy(String paramName, String paramValue);

}
