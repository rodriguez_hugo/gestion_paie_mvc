package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SALAIRE")
public class Salaire implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSalaire;
	private BigDecimal salaireNet;
	private BigDecimal congesPayes;
	private String dateNetPaye;
	
	@Column(name="idEmployeur", nullable = false)
	private long idEmployeur;

	public Salaire(Long idSalaire, BigDecimal salaireNet, BigDecimal congesPayes, String dateNetPaye,
			long idEmployeur) {
		super();
		this.idSalaire = idSalaire;
		this.salaireNet = salaireNet;
		this.congesPayes = congesPayes;
		this.dateNetPaye = dateNetPaye;
		this.idEmployeur = idEmployeur;
	}
	
	public Salaire() {
	}

	public Long getIdSalaire() {
		return idSalaire;
	}

	public void setIdSalaire(Long idSalaire) {
		this.idSalaire = idSalaire;
	}

	public BigDecimal getSalaireNet() {
		return salaireNet;
	}

	public void setSalaireNet(BigDecimal salaireNet) {
		this.salaireNet = salaireNet;
	}

	public BigDecimal getCongesPayes() {
		return congesPayes;
	}

	public void setCongesPayes(BigDecimal congesPayes) {
		this.congesPayes = congesPayes;
	}

	public String getDateNetPaye() {
		return dateNetPaye;
	}

	public void setDateNetPaye(String dateNetPaye) {
		this.dateNetPaye = dateNetPaye;
	}

	public long getIdEmployeur() {
		return idEmployeur;
	}

	public void setIdEmployeur(long idEmployeur) {
		this.idEmployeur = idEmployeur;
	}
	
	
}
