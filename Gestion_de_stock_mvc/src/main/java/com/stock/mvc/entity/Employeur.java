package com.stock.mvc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="EMPLOYEUR")
public class Employeur implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idEmployeur;
	@Column(name="RAISONSOCIALE")
	private String raisonSociale;
	@Column(name="SIRET")
	private String siret;
	@Column(name="CODEAPE")
	private String codeAPE;
	
	@OneToMany(mappedBy="idEmployeur")
	private List<Salaire> salaire;

	public long getIdEmployeur() {
		return idEmployeur;
	}

	public void setIdEmployeur(long idEmployeur) {
		this.idEmployeur = idEmployeur;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public String getCodeAPE() {
		return codeAPE;
	}

	public void setCodeAPE(String codeAPE) {
		this.codeAPE = codeAPE;
	}
	
	public Employeur(long idEmployeur, String raisonSociale, String siret, String codeAPE, List<Salaire> salaire) {
		super();
		this.idEmployeur = idEmployeur;
		this.raisonSociale = raisonSociale;
		this.siret = siret;
		this.codeAPE = codeAPE;
		this.salaire = salaire;
	}
	
	public Employeur() {
		super();

	}

	public List<Salaire> getSalaire() {
		return salaire;
	}

	public void setSalaire(List<Salaire> salaire) {
		this.salaire = salaire;
	}
}
