package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="IMPOT")
public class Impot implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IDIMPOT")
	private Long idImpot;
	@Column(name="NETAVANTIMPOT")
	private BigDecimal netAvantImpot;
	@Column(name="TOTALVERSEEMPL")
	private BigDecimal totalVerseEmpl;
	@Column(name="BASEIMPOT")
	private BigDecimal baseImpot;
	@Column(name="TAUXPERSONALISE")
	private BigDecimal tauxPersonalise;
	@Column(name="MONTANTPRELIMPOT")
	private BigDecimal montantPrelImpot;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "impot_idSalaire",unique=true)
	private Salaire salaire;

	public Long getIdImpot() {
		return idImpot;
	}

	public void setIdImpot(Long idImpot) {
		this.idImpot = idImpot;
	}

	public BigDecimal getNetAvantImpot() {
		return netAvantImpot;
	}

	public void setNetAvantImpot(BigDecimal netAvantImpot) {
		this.netAvantImpot = netAvantImpot;
	}

	public BigDecimal getTotalVerseEmpl() {
		return totalVerseEmpl;
	}

	public void setTotalVerseEmpl(BigDecimal totalVerseEmpl) {
		this.totalVerseEmpl = totalVerseEmpl;
	}

	public BigDecimal getBaseImpot() {
		return baseImpot;
	}

	public void setBaseImpot(BigDecimal baseImpot) {
		this.baseImpot = baseImpot;
	}

	public BigDecimal getTauxPersonalise() {
		return tauxPersonalise;
	}

	public void setTauxPersonalise(BigDecimal tauxPersonalise) {
		this.tauxPersonalise = tauxPersonalise;
	}

	public BigDecimal getMontantPrelImpot() {
		return montantPrelImpot;
	}

	public void setMontantPrelImpot(BigDecimal montantPrelImpot) {
		this.montantPrelImpot = montantPrelImpot;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}

	public Impot(Long idImpot, BigDecimal netAvantImpot, BigDecimal totalVerseEmpl, BigDecimal baseImpot,
			BigDecimal tauxPersonalise, BigDecimal montantPrelImpot, Salaire salaire) {
		super();
		this.idImpot = idImpot;
		this.netAvantImpot = netAvantImpot;
		this.totalVerseEmpl = totalVerseEmpl;
		this.baseImpot = baseImpot;
		this.tauxPersonalise = tauxPersonalise;
		this.montantPrelImpot = montantPrelImpot;
		this.salaire = salaire;
	}

	public Impot() {
		super();
	}
	
}