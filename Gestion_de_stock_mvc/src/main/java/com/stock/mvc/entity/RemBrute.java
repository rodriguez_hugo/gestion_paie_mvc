package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="REMUNERATIONBRUTE")
public class RemBrute implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IDREMBRUTE")
	private Long idRemBrute;
	@Column(name="SALAIREBASE")
	private BigDecimal salaireBase;
	@Column(name="HEURESUPP")
	private BigDecimal heuresupp;
	@Column(name="INDEMNITENS")
	private BigDecimal indeminteNS;
	@Column(name="PRIME")
	private BigDecimal prime;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="REMBRUTE_IDSALAIRE", unique=true)
	private Salaire salaire;

	public Long getIdRemBrute() {
		return idRemBrute;
	}

	public void setIdRemBrute(Long idRemBrute) {
		this.idRemBrute = idRemBrute;
	}

	public BigDecimal getSalaireBase() {
		return salaireBase;
	}

	public void setSalaireBase(BigDecimal salaireBase) {
		this.salaireBase = salaireBase;
	}

	public BigDecimal getHeuresupp() {
		return heuresupp;
	}

	public void setHeuresupp(BigDecimal heuresupp) {
		this.heuresupp = heuresupp;
	}

	public BigDecimal getIndeminteNS() {
		return indeminteNS;
	}

	public void setIndeminteNS(BigDecimal indeminteNS) {
		this.indeminteNS = indeminteNS;
	}

	public BigDecimal getPrime() {
		return prime;
	}

	public void setPrime(BigDecimal prime) {
		this.prime = prime;
	}

	public Salaire getSalaire() {
		return salaire;
	}

	public void setSalaire(Salaire salaire) {
		this.salaire = salaire;
	}

	public RemBrute(Long idRemBrute, BigDecimal salaireBase, BigDecimal heuresupp, BigDecimal indeminteNS,
			BigDecimal prime, Salaire salaire) {
		super();
		this.idRemBrute = idRemBrute;
		this.salaireBase = salaireBase;
		this.heuresupp = heuresupp;
		this.indeminteNS = indeminteNS;
		this.prime = prime;
		this.salaire = salaire;
	}

	public RemBrute() {
		super();
	}
	
}
